# Deploy GitLab to K3s on Civo Cloud

This project help you to:
- Create and manage a Civo Kubernetes cluster from **[Gitpod](https://www.gitpod.io/)**. It uses the **Civo CLI** to create the cluster on **[Civo](https://www.civo.com)**
  - 👋 if you open this project with **GitPod**, you'll get everything you neef (Civo CLI, Kubectl, K9s). That's the magic of **GitPod**
  - [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/gitlab-cookbook/kubernetes/gitlab-on-civo-cluster)
- 🚀 Deploy **GitLab** on **Civo** in less than 10 minutes 🎉

## Prerequisites

### Accounts

- Create an account on **GitLab.com**
- Create an account on **[Civo](https://www.civo.com)**
- Create an account on **[Gitpod](https://www.gitpod.io)**

### On the Gitpod side

- Set the `CIVO_API_KEY` environment variable in the GitPod's settings of your GitPod account
  - Go to https://gitpod.io/variables


### On the GitLab side (this current project)

- Update the `.env` file of this project with the appropriate values (Civo settings):

  ```bash
  CLUSTER_NAME="panda"
  CLUSTER_SIZE="g3.k3s.large"
  CLUSTER_NODES=3
  CLUSTER_REGION=NYC1
  KUBECONFIG=./config/k3s.yaml
  ```

## Create the cluster and deploy GitLab on it

To create the Kubernetes Civo cluster (and install GitLab) and get the config file to connect to the cluster with `kubectl`, run the `create-cluster.sh` script. This script will:

- Create the Kubernetes cluster on Civo
- Get the KUBECONFIG file
- Install GitLab on the created cluster

🖐️ At the end, you'll obtain the url of your new GitLab instance:
- Something like: `GitLab URL: https://gitlab.067de880-38eb-4e9f-a28e-08ba3e11c943.k8s.civo.com`
- You can retrieve all the information in `./config`

Once the cluster created, you can check the progress of the GitLab deployment with these commands:

```bash
kubectl get pods --all-namespaces
# or you can filter on the gitlab namespace
kubectl get pods --namespace gitlab
```

Or you can run **K9s** from the Gitpod terminal:

```bash
k9s --all-namespaces
```
> It's more "user friendly"

Once the GitLab instance deployed, you can get the `root password` with this script: `get-gitlab-secret.sh` and then connect to your new GitLab.


## Good to know

> - If you run `k9s --all-namespaces` in the Gitpod `k9s` terminal, you'll watch the creation of the GitLab runner (and other applications).
> - The `remove-cluster.sh` script allows you to destroy the Civo Kubernetes cluster
> - If you need to get the cluster configuration file again, run the `get-kube-config.sh` script
> - If you need to get the other information again, run the `get-civo-data.sh` script
