#!/bin/bash
set -o allexport; source .env; set +o allexport


URL=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}') 
# Removing ANSI color codes from text stream
NEW_URL=$(echo $URL | sed 's/\x1b\[[0-9;]*m//g')
echo "$NEW_URL"
echo "$NEW_URL" > ./config/url.txt


civo --region=${CLUSTER_REGION} kubernetes show ${CLUSTER_NAME} > ./config/infos.txt

# External IP
EXTERNAL_IP=$(sed '9q;d' ./config/infos.txt)
# DNS A record
DNS=$(sed '10q;d' ./config/infos.txt)

echo "${EXTERNAL_IP#*:}" | xargs > ./config/external_ip.txt
echo "${DNS#*:}" | xargs > ./config/dns.txt

GITLAB_URL="https://gitlab.$(echo "${DNS#*:}" | xargs)"

echo GITLAB_URL > ./config/gitlab_url.txt
echo "GitLab URL: ${GITLAB_URL}"
