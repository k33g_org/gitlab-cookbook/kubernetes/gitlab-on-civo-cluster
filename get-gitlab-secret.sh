#!/bin/bash
set -o allexport; source .env; set +o allexport

kubectl get secret gitlab-gitlab-initial-root-password --namespace gitlab -ojsonpath='{.data.password}' | base64 --decode ; echo